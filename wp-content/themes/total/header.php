<?php
/**
 * The header for our theme.
 *
 * @package Total
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet"> 

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="ht-page">
	<header id="ht-masthead" class="ht-site-header">
		<div class="ht-container ht-clearfix">
			<div id="ht-site-branding">
				<?php 
				if ( function_exists( 'has_custom_logo' ) && has_custom_logo() ) :
					the_custom_logo();
				else : 
					if ( is_front_page() ) : ?>
						<h1 class="ht-site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="ht-site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php endif; ?>
					<p class="ht-site-description"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'description' ); ?></a></p>
				<?php endif; ?>
			</div><!-- .site-branding -->
        <div class="call">
		  <a href="tel:+6562946688"><span class="phone">call:+65 6294 6688</span></a>
	  <a href="mailto:awg@awginsurance.com"><span class="email">awg@awginsurance.com</span></a>
<style>
.click-to-toggle{
    cursor: pointer
}
</style>

<span class="click-to-toggle"><i class="fa fa-search"></i></span>

<span style="display:none">
 <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
</span>

<script>
var button = document.querySelector('.click-to-toggle');

button.addEventListener('click', function (event) {

    var next = this.nextElementSibling;

    if (next.style.display == "none") {
        next.style.display = "block";

    } else {
        next.style.display = "none";
    }
});
</script>
	</div>
			<nav id="ht-site-navigation" class="ht-main-navigation">
				<div class="toggle-bar"><span></span></div>
				<?php 
				wp_nav_menu( array( 
					'theme_location' => 'primary', 
					'container_class' => 'ht-menu ht-clearfix' ,
					'menu_class' => 'ht-clearfix',
					'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				) ); 
				?>
			</nav><!-- #ht-site-navigation -->
		</div>
	</header><!-- #ht-masthead -->

	<div id="ht-content" class="ht-site-content ht-clearfix">