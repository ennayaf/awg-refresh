<?php
/**
 * Template Name: Corporate
 *
 * @package Total
 */
?>
<?php get_header(); ?>

<?php

$page_comment = $total_options['page_comment'];



// Calculate sidebar column & content column

$sidebar_col = 'col-md-3';

if ($sidebar_width == 'large') {

	$sidebar_col = 'col-md-4';

}



$content_col_number = 12;

if (is_active_sidebar($left_sidebar) && (($sidebar == 'both') || ($sidebar == 'left'))) {

	if ($sidebar_width == 'large') {

		$content_col_number -= 4;

	} else {

		$content_col_number -= 3;

	}

}

if (is_active_sidebar($right_sidebar) && (($sidebar == 'both') || ($sidebar == 'right'))) {

	if ($sidebar_width == 'large') {

		$content_col_number -= 4;

	} else {

		$content_col_number -= 3;

	}

}



$content_col = 'col-md-' . $content_col_number;

if (($content_col_number == 12) && ($layout_style == 'full')) {

	$content_col = '';

}



$main_class = array('site-content-page');



if ($content_col_number < 12) {

    $main_class[] = 'has-sidebar';

}

?>

<?php

/**

 * @hooked -total_page_heading - 5

 **/

do_action('total_before_page');

?>

<main  class="<?php echo join(' ',$main_class) ?>">

	<?php if ($layout_style != 'full'): ?>

	<div class="<?php echo esc_attr($layout_style) ?> clearfix">

		<?php endif;?>

		<?php if (($content_col_number != 12) || ($layout_style != 'full')): ?>

		<div class="row clearfix">

			<?php endif;?>

			<?php if (is_active_sidebar( $left_sidebar ) && (($sidebar == 'left') || ($sidebar == 'both'))): ?>

				<div class="sidebar left-sidebar <?php echo esc_attr($sidebar_col) ?> hidden-sm hidden-xs sidebar-<?php echo esc_attr($sidebar_width); ?>">

					<?php dynamic_sidebar( $left_sidebar );?>

				</div>

			<?php endif;?>

			<div class="site-content-page-inner <?php echo esc_attr($content_col) ?>">

				<div class="page-content">




        <header class="site-header homepage-classic-header">

       

        <div class="col-md-8 no-padding slider">      

        <!-- <div class="item-image"><img src="assets/images/AWG.png" alt="Home slider image" class="img-responsive"></div> -->

        <?php //echo do_shortcode('[rev_slider alias="home-page"]'); ?>       

        </div>

        <div class="col-md-4 no-padding client">

            <?php 

        $sorted = array();

				$args   = array( 'hide_empty' => false,);

				$taxarray = array ('insurance','personal_insurance');

				$terms  = get_terms( $taxarray, $args );


				if( $terms ) : 

				    foreach ( $terms as $term ) {

				        $sorted[]  = array(

				            'order' => get_field( 'start_date', $term ), // Get field from ACF

				            'name'  => $term->name,

				            'image' => get_field('icon_',$term),

                     // 'order' => 'ASC',
				            'f_img' => get_field('image_hover', $term),

				            'link'  => $term->term_id

				        );		



				    }



				    function sortByOrder($a, $b) {

				        return $a['order'] - $b['order'];

				    }

				   usort($sorted, 'sortByOrder');

				    $i=0;

				    foreach( $sorted as $t ) :				    

						$i++;

                         echo '<div class="col-md-6 col-xs-6 contenthere">

                                <a class="nohover" href="">

                                  <img src="'.$t['image'].'">' . $t['name'] .'

                                </a>';

                         //echo '<div class="col-md-12 col-xs-12"><a href="' . esc_url( get_term_link( $term ) ) . '"><img class="fea_img" src="'.$f_img.'" alt=""/>' . $term->name .'</a></div>';

                          if($i%2 != 0){

                            echo '<a class="whenhover" href="' . esc_url( get_term_link( $t['link'] ) ) .'" style="background: #ffffff url(\''.$t['f_img'].'\') no-repeat scroll right top; position: absolute; top: 0px; float: right; height: 100% ! important; z-index: 1; right: -102% ! important; width: 202% ! important;"><span style="float: left; width: 50%; margin-top: 50px;">' . $t['name'] .'</span></a>

                              </div>';

                          }else{

                            echo '<a class="whenhover" href="' . esc_url( get_term_link( $t['link'] ) ) .'" style=" background: #ffffff url(\''.$t['f_img'].'\') no-repeat scroll left top; position: absolute; top: 0px; float: right; height: 100% ! important; z-index: 1; right: 0px ! important; width: 202% ! important;"><span style="width: 50%; float: right; margin-top: 50px;">' . $t['name'] .'</span></a>

                              </div>';

                          }

				      	

				    endforeach;

				endif;

         ?>

          



          </div>     



      </header>

      <!--end site header-->

      <section class="no-padding">

        <div class="service-section-04">      

            <div class="col-md-6 no-padding service-item-wrapper">

              <?php 

              $args=array('post_type'=>'post','posts_per_page'=>'1');

              $the_query= new WP_Query($args);

                if ($the_query->have_posts()) {

                  while ($the_query->have_posts()) {

                    $the_query->the_post();

                    echo '<h3 class="Journey">' . get_the_title() . '</h3>';

                    echo '<p>'.the_excerpt().'</p>';

                    echo '<a href="http://www.awginsurance.com/awg-insurance-brokers/" class="read">read more ></a>';

                  }

                }else{

                  echo "no posts";

                }

               ?>

              </div>

               <?php

          // Start the Loop.

               $count=0;

                while (have_posts()) : the_post();

                    // Include the page content template.

                   //total_get_template('content', 'page');

                    if( have_rows('boxx') ): while( have_rows('boxx') ): the_row();?>   

                    <?php $bgs=get_sub_field('background');

                          foreach ($bgs as $bg) {

                            if ($bg['image']) {

                              echo '<div class="col-md-3 no-padding service-item-wrapper box-'.$count++.'" style="background:url('.$bg['image'].');"><div class="padding">';

                            }else{

                              echo '<div class="col-md-3 no-padding service-item-wrapper box-'.$count++.'" style="background:'.$bg['color'].';"><div class="padding">';

                            }

                           }

                        ?>                         

                        <h3><?php echo get_sub_field('title'); ?></h3>

                          <?php if(get_sub_field('content')):

                          echo get_sub_field('content');

                          endif;?> 



                        <?php $buttons=get_sub_field('button');

                        foreach ($buttons as $button) {

                         echo '<a href="'.$button["url"].'"><span>'.$button["text"].'</span></a>';

                        }

                        ?>  

                    </div>                           

                    



              <?php endwhile;endif; 

          endwhile;

          ?>      

        </div>

        </section>

				</div>

                <?php if ($page_comment == 1) {

                    comments_template();

                } ?>

			</div>

			<?php if (is_active_sidebar( $right_sidebar ) && (($sidebar == 'right') || ($sidebar == 'both'))): ?>

				<div class="sidebar right-sidebar <?php echo esc_attr($sidebar_col) ?> hidden-sm hidden-xs sidebar-<?php echo esc_attr($sidebar_width); ?>">

					<?php dynamic_sidebar( $right_sidebar );?>

				</div>

			<?php endif;?>

			<?php if (($content_col_number != 12) || ($layout_style != 'full')): ?>

		</div>

	<?php endif;?>

		<?php if ($layout_style != 'full'): ?>

	</div>

<?php endif;?>

</main>

<?php get_footer(); ?>