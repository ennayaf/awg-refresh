<?php
/**
 * The template for displaying all pages.
 *
 * @package Total
 */

get_header(); ?>

<header class="ht-main-header">
	<div class="ht-container">
	<?php 
	if( is_page( array( 'contact-us' ) ) ){
	echo '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.7440062321652!2d103.87346571475398!3d1.32964729903023!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da1786ec0d0c9f%3A0x66fca0f7c9ba76a5!2sPines+Industrial+Building+Rental!5e0!3m2!1sen!2ssg!4v1495770884990" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';
	} else {
			the_post_thumbnail();  
	}
?>
	</div>
</header><!-- .entry-header -->

<div class="ht-container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
	<?php the_title( '<h1 class="ht-main-title">', '</h1>' ); ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
</div>

<?php get_footer();
