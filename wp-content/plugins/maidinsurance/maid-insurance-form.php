<?php
/*
Plugin Name: Maid Insurance
Description: A custom plugin for AWG to sell maid insurance policy
Author: TechTicks
*/
function wpb_adding_scripts() { 
    //Load all the js files
    wp_enqueue_script( 'validator.js', plugin_dir_url( __FILE__ ) . '_inc/js/validator.js', array('jquery'), null, false );
    wp_enqueue_script( 'maid-insurance.js', plugin_dir_url( __FILE__ ) . '_inc/js/maid-insurance.js', array('jquery'), null, false );
    wp_enqueue_script( 'jquery.easing.min.js', plugin_dir_url( __FILE__ ) . '_inc/js/jquery.easing.min.js', array('jquery'), null, false );
    wp_enqueue_script( 'bootstrap.min.js', get_template_directory_uri() . '/js/bootstrap.min.js', array(),null,false );

    //Load all the css files
    wp_enqueue_style( 'bootstrap.min.css', get_template_directory_uri() . '/css/bootstrap.min.css', array(),null,false );
    wp_enqueue_style( 'maid-insurance.css', plugin_dir_url( __FILE__ ) . '_inc/css/maid-insurance.css', array(),null,false );
    wp_enqueue_style( 'msform.css', plugin_dir_url( __FILE__ ) . '_inc/css/msform.css', array(),null,false );
}
add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );  

include 'maid-insurance-admin.php';

add_shortcode('multipage_form_sc','multipage_form');

function multipage_form(){
    echo'
        <form id = "maid_form" name = "maid_form" data-toggle="validator" role="form">';
                display_initialForm();
    echo'
            <fieldset id = "priceTable">
            <div id = "pricingTable"></div>
            </fieldset>
            <fieldset id = "addOns">
            <div id = "addOns">';
                display_personal_addons();
    echo'
            </div>
            </fieldset>
            <fieldset id = "personalDetails">
            <div id = "personalDetails">';
                display_personal_form();
    echo'
            </div>
            </fieldset>
            <fieldset id = "orderSummary">
            <div id = "orderSummary"></div>
            </fieldset>
        </form>';
}

function display_initialForm(){
    echo'
        <fieldset id = "initialForm">
            <div id = "statement-form">
            <div class="form-group statement">
                <label id = "statement"> I need a maid insurance policy starting from </label>
                <div id="block">
                    <input type="date" name="start_date"
                                id="start_date" class="form-control statement" required/>
                    <div class="help-block with-errors"></div>
                </div>
            </div>

            <div class="form-group statement">
                <br/>
                <label id = "statement">For </label>
                <div id="block">
                    <select name="duration" id="duration" class="form-control statement" required/>
                        <option value="" selected>Please Select</option>
                        <option value="14 Months">14 Months</option>
                        <option value="26 Months">26 Months</option>
                    </select> .
                    <div class="help-block with-errors"></div>
                </div>
            </div>

            <div class="form-group statement">
                <label id = "statement">My maid is from </label>
                <div id="block">
                    <select name="nationality" id = "nationality" class="form-control statement" required/>
                        <option value="" selected>Please Select</option>
                        <option value="Philippines">Philippines</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="India"> India</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="South Korea">South Korea</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="Taiwan">Taiwan</option>
                        <option value="Thailand">Thailand</option>
                    </select>
                    <label id = "statement"> .</label>
                    <div class="help-block with-errors"></div>
                </div>
            </div>

            <br/>
            <br/>
            <br/>

            <label style="font-size:14px">Promo Code (if applicable):</label>
            <input type="text" class="form-control statement" name="promo_code" id="promo_code"/>

            <br/>
            <br/>

            <input type="button" name="submit" class="submit action-button" onclick="showPricing();" data-minDate value = "Next"/>

            <br/>
            <hr/>

            <b>Tips</b>

            <ul>
                <li>
                    When should I start renewing my Domestic Workers Insurance
                    <span class="glyphicon glyphicon-question-sign" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="AWG to provide copy"></span>
                </li>
                <li>
                    Is your Domestic Workers passport still valid through the renewal period
                    <span class="glyphicon glyphicon-question-sign" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="AWG to provide copy"></span>
                </li>
                <li>
                    What if my Domestic Worker is going for her home leave before continuing her contract with me
                    <span class="glyphicon glyphicon-question-sign" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="AWG to provide copy"></span>
                </li>
            </ul>

            <script type="text/javascript">
                jQuery(function () {
                    jQuery(\'[data-toggle="tooltip"]\').tooltip()
                })
            </script>
            </div>
        </fieldset>';
}


function display_personal_addons(){
    echo'
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col" style="width: 70%">Add-On</th><td>Premium</td><td></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><b>Indemnity Buy Back </b>
                        <br/>Waiver of Indemnity for the forfeiture of $5000 security deposit to Ministry of Manpower, in the event the forfeiture is due to breach of conditions by the insured Maid.ss</td>
                        <td>$50</td>
                        <td><input type ="checkbox" name="add_on" value= "1"></input></td>
                    </tr>
                    <tr>
                        <td><b>Daily Hospital Allowance Benefit </b>
                        <br/>Receive Maid Allowance of $20 per day up to 15days if your Domestic Helper is hospitalised for 3 or more days due to an injury or illness. </td>
                        <td>$6</td>
                        <td><input type ="checkbox" name="add_on" value= "2"></input></td>
                    </tr>
                    <tr>
                        <td><b>Daily Child/Elder Care Allowance Benefit</b>
                        <br/>Receive Additional Allowance of $30 per day if you have Children below 12 years old, Elderly Above 65 years old when your Maid is Hospitalised. </td>
                        <td>$30</td>
                        <td><input type ="checkbox" name="add_on" value= "3"></input></td>
                    </tr>
                    <tr>
                        <td><b>Outpatient Medical Expenses Due to Illness</b>
                        <br/>Receive up to $30 per visit after you have paid $10 if your maid is not well and needs to visit the General Practioner. </td>
                        <td>$78</td>
                        <td><input type ="checkbox" name="add_on" value= "4"></input></td>
                    </tr>
                    <tr>
                        <td><b>Top up Hospital & Surgical Expenses</b>
                        <br/>Add additional coverage amount:
                        <select id="benefit" name="benefit" />
                            <option value="Please Select" selected>Please Select</option>
                            <option value="5000">$5000</option>
                            <option value="10000">$10000</option>
                            <option value="15000">$15000</option>
                            <option value="20000">$20000</option>
                            <option value="25000">$20000</option>
                        </select>
                        </td>
                        <td><div id="additional_amt">$--</div></td>
                        <td><input id ="add_cb" name="add_on" type ="checkbox" disabled></input></td>
                    </tr>
                    <tr>
                      <td><b>Home Content</b>
                      <br/>Insure your Home Content against theft by your Maid.</td>
                      <td>$50</td>
                      <td><input type ="checkbox" name="add_on" value= "10"></input></td>
                    </tr>
                </tbody>
            </table>
            <input type = "button" class="next action-button" onclick="showNext(\'#addOns\')" value = "Next"/>

            <script type="text/javascript">
                jQuery(function() {
                    jQuery(\'#benefit\').change(function(){
                    alert(\'hi\');
                        if (jQuery(\'#benefit\').val()=="5000"){
                            jQuery(\'#additional_amt\').text("$30");
                            jQuery(\'#add_cb\').removeAttr("disabled");
                            jQuery(\'#add_cb\').val("5");
                        }else if(jQuery(\'#benefit\').val()=="10000"){
                            jQuery(\'#additional_amt\').text("$55");
                            jQuery(\'#add_cb\').removeAttr("disabled");
                            jQuery(\'#add_cb\').val("6");
                        }else if(jQuery(\'#benefit\').val()=="15000"){
                            jQuery(\'#additional_amt\').text("$80");
                            jQuery(\'#add_cb\').removeAttr("disabled");
                            jQuery(\'#add_cb\').val("7");
                        }else if(jQuery(\'#benefit\').val()=="20000"){
                            jQuery(\'#additional_amt\').text("$105");
                            jQuery(\'#add_cb\').removeAttr("disabled");
                            jQuery(\'#add_cb\').val("8");
                        }else if(jQuery(\'#benefit\').val()=="25000"){
                            jQuery(\'#additional_amt\').text("$130");
                            jQuery(\'#add_cb\').removeAttr("disabled");
                            jQuery(\'#add_cb\').val("9");
                        }else if(jQuery(\'#benefit\').val()=="Please Select"){
                            jQuery(\'#additional_amt\').text("--");
                            jQuery(\'#add_cb\').attr("disabled", true);
                            jQuery(\'#add_cb\').prop(\'checked\', false)
                        }
                    });
                });
            </script>';
}

function display_personal_form(){
    echo'
            <h3> Personal Particulars</h3>
            <hr/>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="given_name" id="given_name_lbl">Given Name</label>
                <input type="text" name="given_name" id="given_name" class="form-control" required/>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group col-md-6">
                <label for="sur_name" id="sur_name_lbl">Surname</label>
                <input type="text" name="sur_name" id="sur_name" class="form-control" required/>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group col-md-12">
                <label for="email" id="email_lbl">Email</label>
                <input type="email" name="email" id="email" class="form-control" required/>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group col-md-12">
                <label for="phone_number" id="phone_number_lbl">Phone Number</label>
                <div class="input-group">
                    <div class="input-group-addon">+65</div>
                    <input type="text" name="phone_number" id="phone_number" class="form-control" required/>
                </div>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group col-md-12">
                <label for="dob" id="dob_lbl">Date of Birth</label>
                <input type="date" name="dob" id="dob" class="form-control" required/>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group col-md-12">
                <label for="nric" id="nric_lbl">NRIC/FIN</label>
                <input type="text" name="nric" id="nric" class="form-control" required/>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="unit_no" id="unit_no_lbl">Unit No.</label>
                    <input type="text" name="unit_no" id="unit_no" class="form-control"/>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group col-md-4">
                    <label for="building" id="building_lbl">Building Name(if any)</label>
                    <input type="text" name="building" id="building" class="form-control"/>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group col-md-4">
                    <label for="street_no" id="street_no_lbl">Street</label>
                    <input type="text" name="street_no" id="street_no" class="form-control" required />
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group col-md-2">
                    <label for="postal_code" id="postal_code_lbl">Postal Code</label>
                    <input type="text" name="postal_code" id="postal_code" class="form-control" required />
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group col-md-12">
                    <label for="policy_start_date" id="policy_start_date_lbl">Policy Start Date</label>
                    <input type="date" name="policy_start_date" id="policy_start_date" class="form-control" value="" data-minDate required/>
                    <div class="help-block with-errors"></div>
                </div>
            </div>

            <br/>
            <br/>

            <div id = "maid" style="padding-top:5px">
                <h3> Maid Particulars</h3>
                <hr/>

                <div class="form-row">
                    <div class="form-group col-md-6">
                    <label for="given_name" id="given_name_lbl">Given Name</label>
                    <input type="text" name="maid_given_name" id="maid_given_name" class="form-control" required/>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group col-md-6">
                    <label for="sur_name" id="sur_name_lbl">Surname</label>
                    <input type="text" name="maid_sur_name" id="maid_sur_name" class="form-control" required/>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group col-md-12">
                    <label for="passport" id="passport_lbl">Passport No.</label>
                    <input type="text" name="passport" id="passport" class="form-control" required/>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group col-md-12">
                    <label for="work_permit" id="work_permit_lbl">Work Permit No.</label>
                    <input type="text" name="work_permit" id="work_permit" class="form-control" required/>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group col-md-12">
                    <label for="work_permit_type" id="work_permit_type_lbl">Work Permit Type</label>
                    <select class="form-control" name="work_permit_type" id="work_permit_type" required/>
                        <option value ="" selected>Please Select</option>
                        <option>New</option>
                        <option>Transfer</option>
                        <option>Renewal</option>
                        <option>Not Sure</option>
                    </select>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group col-md-12">
                <label for="dob_maid" id="dob_maid_lbl">Date of Birth</label>
                <input type="date" name="dob_maid" id="dob_maid" class="form-control" required/>
                <div class="help-block with-errors"></div>
            </div>

            <div class="form-group col-md-12">
                <label for="sb_trans" id="sb_trans_lbl">SB Transmission No.</label>
                <input type="text" name="sb_trans" id="sb_trans" class="form-control" required />
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <input type="button" name="submit" class="submit action-button" onclick="showNext(\'#personalDetails\')" value = "Next"/>';
}

include 'maid-insurance-form-handler.php';
?>
