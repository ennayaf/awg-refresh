<?php

    add_action('admin_menu', 'maidinsurance_admin_menu');
    
    function maidinsurance_admin_menu(){
        add_menu_page( 'Maid Insurance', 'Maid Insurance', 'manage_options', 'maidinsurance-admin.php', 'maidinsurance_admin', 'dashicons-universal-access-alt', 6  );
        add_submenu_page( 'maidinsurance-admin.php', 'Policy Pricing', 'Policy Pricing', 'manage_options', 'maidinsurance/maidinsurance-admin-policy-pricing.php', 'maidinsurance_admin_pricing' ); 
        add_submenu_page( 'maidinsurance-admin.php', 'Other Settings', 'Other Settings', 'manage_options', 'maidinsurance/maidinsurance-admin-other-settings.php', 'maidinsurance_admin_settings' ); 
    }
    
    function maidinsurance_admin(){
        echo'
        <link rel="stylesheet" type="text/css" href="'.plugin_dir_url( __FILE__ ) .'_inc/css/datatables.css">
        <script type="text/javascript" charset="utf8" src="'.plugin_dir_url( __FILE__ ) . '_inc/js/datatables.js"></script>

        <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery(\'#orders\').DataTable();
        } );
        </script>' ;

        echo'<div class="wrap">
        <h2>Customer Orders</h2>
        <hr>
        </div>';
        global $wpdb;
        
        $form_results = $wpdb->get_results( "SELECT * FROM mi_orders 
        INNER JOIN mi_policy_pricing
        ON mi_orders.pricing_id = mi_policy_pricing.pricing_id
        INNER JOIN mi_policy_type
        ON mi_policy_type.policy_id = mi_policy_pricing.policy_id
        INNER JOIN mi_policy_duration
        ON mi_policy_duration.duration_id = mi_policy_pricing.duration_id" );
        
        echo '
        <table id="orders" class="cell-border nowrap" style="font-size:10px">
        <thead>
        <tr>
        <th>Timestamp</th>
        <th>Purchase</th>
        <th>Full Name</th>
        <th>DOB</th>
        <th>NRIC</th>
        <th>Address</th>
        <th>Start Date</th>
        <th>Maid Details</th>
        <th>Promo Code</th>
        <th>Download</th>
        </tr>
        </thead>
        <tbody>';
        
        foreach ($form_results as $form_results) {
            echo '
            <tr>
            <td>' .$form_results->timestamp. '</td>
            <td>' .$form_results->name.' ' .$form_results->duration. '</td>
            <td>' .$form_results->given_name. ' ' .$form_results->sur_name. '</td>
            <td>' .$form_results->dob. '</td>
            <td>' .$form_results->nric. '</td>
            <td>' .$form_results->unit_no. ' 
            <br/>' .$form_results->building. ' 
            <br/>' .$form_results->street. ' 
            <br/>' .$form_results->postal_code. '</td>
            <td>' .$form_results->start_date. '</td>
            <td>' .$form_results->maid_given_name. ' ' .$form_results->maid_sur_name. '
            <br/>' .$form_results->nationality. '
            <br/>' .$form_results->maid_dob. '
            <br/>' .$form_results->passport_no. '
            <br/>' .$form_results->work_permit_no. '
            <br/>' .$form_results->work_permit_type. '
            <br/>' .$form_results->sb_transmission_no. '</td>
            <td>' .$form_results->promo_code. '</td>
            <td><button>download</button></td>
            </tr>
            ';
        }

        echo'
        </tbody>
        </table>';
        
    }
    
    function maidinsurance_admin_pricing(){
        echo'<div class="wrap">
        <h2>Welcome To My Pricing</h2>
        </div>';
    }
    
    function maidinsurance_admin_settings(){
        echo'<div class="wrap">
        <h2>Welcome To My Settings</h2>
        </div>';
    }
    ?>
