<?php

add_action( 'rest_api_init', function () {
    register_rest_route( 'maidinsurance/v1', '/pricingTable', array(
        'methods' => WP_REST_Server::EDITABLE,
        'callback' => 'pricingTable',
    ) );
} );

function pricingTable(){
    // Retrieve data from Query String
    $data= $_POST['str'];
    $k = stripslashes(html_entity_decode($data));
    $l = json_decode($k,true);

    //debug_to_console($k);

    $date= $l["start_date"];
    $duration = $l["duration"];
    $nationality = $l["nationality"];

    global $wpdb;

    $type_results = $wpdb->get_results( "SELECT name FROM mi_policy_type" );

    //if philippines make category A, else it is B
    $category = 'A';

    if($nationality!='Philippines'){
        $category = 'B';
    }

    //debug_to_console($category);
    $pricing_results = $wpdb->get_results( "SELECT * FROM mi_policy_pricing
            inner join mi_policy_type
            on mi_policy_pricing.policy_id = mi_policy_type.policy_id
            inner join mi_policy_duration
            on mi_policy_pricing.duration_id = mi_policy_duration.duration_id
            where mi_policy_duration.duration like '".$duration."'
             and mi_policy_pricing.category like '".$category ."'" );

    $policy_types = array();
    $pricing_arr = array();
    $benefit_arr = array();
    $outpatient_arr = array();
    $hospitalisation_arr = array();
    $rehiring_arr = array();
    $liabilties_arr = array();
    $damage_arr = array();
    $policy_id_arr = array();
    $policies = array();

    foreach ($type_results as $type_results) {
        $policy_types[]= $type_results->name;
    }

    foreach ($pricing_results as $pricing_results) {
        $pricing_arr[]= $pricing_results->pricing;
        $benefit_arr[]= $pricing_results->benefits;
        $outpatient_arr[]= $pricing_results->outpatient;
        $hospitalisation_arr[] = $pricing_results->hospitalisation;
        $rehiring_arr[]= $pricing_results->rehiring;
        $liabilties_arr[]= $pricing_results->liabilities;
        $damage_arr[]= $pricing_results->damage_theft;
        $policy_id_arr[]= $pricing_results->pricing_id;
    }
    $policies['prices'] = $pricing_arr;
    $policies['benefits'] = $benefit_arr;
    $policies['outpatient'] = $outpatient_arr;
    $policies['hospitalisation'] = $hospitalisation_arr;
    $policies['rehiring'] = $rehiring_arr;
    $policies['liabilities'] = $liabilties_arr;
    $policies['damage'] = $damage_arr;
    $policies['types'] = $policy_types;
    $policies['id'] = $policy_id_arr;


    $tempStr =  json_encode($policies);
    return $tempStr;
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'maidinsurance/v1', '/showSummary', array(
        'methods' => WP_REST_Server::EDITABLE,
        'callback' => 'showSummary',
    ) );
} );

function showSummary(){
    $start_date = array("Peter"=>65, "Harry"=>80, "John"=>78, "Clark"=>90);

    $tempStr =  json_encode($start_date);
    return $tempStr;
}

function debug_to_console( $d ) {
    $output = $d;
    if ( is_array( $output )){
        //$output = implode( ',', $output);
        //echo $output.toString();
    }

    echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}

?>
