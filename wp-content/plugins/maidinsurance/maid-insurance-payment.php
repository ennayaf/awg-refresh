<?php
add_action( 'rest_api_init', function () {
    //register_rest_route( 'myplugin/v1', '/makepayment/(?P<id>[\d]+)', array(
    register_rest_route( 'maidinsurance/v1', '/makepayment', array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'makePayment',
    ) );
} );



function makePayment( $data ) {
    $SGEPAY_URLStaging = 'https://staging.sgepay.com:8008/apisubmit.aspx';
    $SGePAY_MID = 'awgtestAPI';
    $SGePAY_RequestId = "sTWsBrgDkT5N";
    $SGePAY_Amount = 128;
    $SGePAY_Email = 'test@test.com';
    $SGePAY_BillNo = '1';

    $return_json = ["MID"=>"awgtestAPI",
                    "amount"=>$SGePAY_Amount,
                    "requestId"=>"sTWsBrgDkT5N",
                    "email"=>$SGePAY_Email,
                    "BillNo" => $SGePAY_BillNo];

    return json_encode($return_json);

}

add_action( 'rest_api_init', function () {
    register_rest_route( 'maidinsurance/v1', '/paymentCallback', array(
        'methods' => WP_REST_Server::EDITABLE,
        'callback' => 'handlePayment',
    ) );
} );

function handlePayment($data){
    //TO-DO
    return true;
}
?>
