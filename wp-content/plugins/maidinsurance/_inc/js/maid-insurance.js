//THIS IS TO SHOW THE PRICING TABLE WITH THE DIFF INSURANCE PLANS
function showPricing(){
    showNext('#initialForm');
    var start_date = jQuery('#start_date').val();
    var duration = jQuery('#duration').val();
    var nationality = jQuery('#nationality').val();
    var queryString = "?start_date=" + start_date ;
    queryString +=  "&duration=" + duration + "&nationality=" + nationality;

    let selection = {
        start_date: start_date,
        duration: duration,
        nationality: nationality
    };

    var selection_json = JSON.stringify(selection);
    console.log(selection_json);

    jQuery.ajax({
        url: 'wp-json/maidinsurance/v1/pricingTable',
        method: 'POST',
        dataType: 'json',
        data: {
            str: selection_json,
        },
        success: function(result) {
            var plan_rt = JSON.parse(result);
            var html = '<table class="table table-striped">\
            <thead><tr><th scope="col"></th>';

            for(var i=0; i<plan_rt.types.length;i++) {
                html+='<th scope="col">';
                html+=plan_rt.types[i];
                html+='</th>';
            }
            html+='</tr></thead><tbody><tr><td>Price</td>';

            for(var i=0; i<plan_rt.prices.length; i++) {
                html+='<td>';
                html+=plan_rt.prices[i];
                html+='</td>';
            }

            html+='</tr><tr><td>Benefits</td>';
            html+='<td colspan = 3>';
            html+=plan_rt.benefits[0];
            html+='</td>';

            html+='</tr><tr><td>Outpatient expenses due to Accident</td>';
            for(var i=0; i<plan_rt.outpatient.length; i++) {
                html+='<td>';
                html+=plan_rt.outpatient[i];
                html+='</td>';
            }

            html+='</tr><tr><td>Hospitalisation & surgical expenses per year</td>';
            for(var i=0; i<plan_rt.hospitalisation.length; i++) {
                html+='<td>';
                html+=plan_rt.hospitalisation[i];
                html+='</td>';
            }

            html+='</tr><tr><td>Re-hiring expenses</td>';
            for(var i=0; i<plan_rt.rehiring.length; i++) {
                html+='<td>';
                html+=plan_rt.rehiring[i];
                html+='</td>';
            }

            html+='</tr><tr><td>Liability to third parties</td>';
            for(var i=0; i<plan_rt.liabilities.length; i++) {
                html+='<td>';
                html+=plan_rt.liabilities[i];
                html+='</td>';
            }

            html+='</tr><tr><td>Damage or theft of maid\'s personal belongings</td>';
            for(var i=0; i<plan_rt.damage.length; i++) {
                html+='<td>';
                html+=plan_rt.damage[i];
                html+='</td>';
            }

            html+='</tr></thead><tbody><tr><td>Price</td>';

            for(var i=0; i<plan_rt.id.length; i++) {
                html+='<td><input type="radio" name="plan" value="';
                html+=plan_rt.id[i];
                html+='"/></td>';
            }

            html+='</tr></table><input type="button" name="submit" class="submit action-button" onclick="showNext(\'#priceTable\')" value = "Next"/>';

            console.log(html);
            jQuery('#pricingTable').append(html);
        },
        error: function(error) {
            // check error object or return error
            console.log("error");
        }
    });
}

//THIS IS TO SHOW THE ADDONS PAGE
function showSummary(){
    var selected = [];

    jQuery('div#addOns input[type=checkbox]').each(function() {
        if (jQuery(this).is(":checked")) {
            console.log("yo checkbox");
            selected.push(jQuery(this).attr('value'));
        }
    });

    var selected_plan = jQuery('div#pricingTable input[type=radio]').val();
    console.log(selected_plan+"hi");

    let selection = {
        plan: selected_plan,
        add_ons: selected
    };

    var selection_json = JSON.stringify(selection);
    console.log(selection_json);

    jQuery.ajax({
        type: 'post',
        url: 'wp-json/maidinsurance/v1/showSummary',
        data: 'add_ons=' + selection_json,
        success: function(result) {
            console.log(JSON.parse(result));
            var plan_rt = JSON.parse(result);
            jQuery('#orderSummary').append(plan_rt.Peter);
        },
        error: function(error) {
            // check error object or return error
        }
    });
}

function showNext(divName) {
    //jQuery time
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    alert("clicked!");
    if(animating) return false;
    animating = true;

    current_fs = jQuery(divName);
    console.log(current_fs);
    next_fs = current_fs.next();
    console.log(next_fs);
    //activate next step on progressbar using the index of next_fs
    jQuery("#progressbar li").eq(jQuery("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50)+"%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
                'transform': 'scale('+scale+')',
                'position': 'absolute'
            });
            next_fs.css({'left': left, 'opacity': opacity});
        },
        duration: 800,
        complete: function(){
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
}

function goBack(){
    //jQuery time
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    if(animating) return false;
    animating = true;

    current_fs = jQuery(this).parent();
    previous_fs = jQuery(this).parent().prev();

    //de-activate current step on progressbar
    jQuery("#progressbar li").eq(jQuery("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1-now) * 50)+"%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
        },
        duration: 800,
        complete: function(){
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
}

/*
    jQuery(".submit").click(function(){
        return false;
    });*/
