<?php

function multipage_form(){
    $this_page	=	$_SERVER['REQUEST_URI'];
    $page	=	$_POST['page'];
    if ( $page == NULL ) {
        //show quotation statement page
        displayPage1();
    }
    elseif ( $page == 1 ) {
        //show pricing table
        displayPage2();
    } elseif( $page == 2 ) {
        //show addons
        displayPage3();
    } elseif( $page == 3 ) {
        //show particulars form
        displayPage4();
    }else{
        //show order summary
        displayPage5();
    }
}

function displayPage1(){
    echo '

            <div id = "statement-form">
                <form name = "maid_form" method="post" action="' . $this_page .'" data-toggle="validator" role="form">
                    <div class="form-group statement">
                        <label id = "statement"> I need a maid insurance policy starting from </label>
                            <div id="block">
                                <input type="date" name="start_date" class="form-control statement" data-minDate required/>
                            <div class="help-block with-errors"></div>
                            </div>
                    </div>
                    <div class="form-group statement">
                        <br/><label id = "statement">For </label>
                            <div id="block">
                                <select name="duration" class="form-control statement" required/>
                                <option value="" selected>Please Select</option>
                                <option value="14 Months">14 Months</option>
                                <option value="26 Months">26 Months</option>
                                </select> .
                            <div class="help-block with-errors"></div>
                            </div>
                    </div>
                <div class="form-group statement">
                    <label id = "statement">My maid is from </label>
                    <div id="block">
                        <select name="nationality" class="form-control statement" required/>
                        <option value="" selected>Please Select</option>
                        <option value="Philippines">Philippines</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="India"> India</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="South Korea">South Korea</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="Taiwan">Taiwan</option>
                        <option value="Thailand">Thailand</option>
                        </select><label id = "statement"> .</label>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            <br/>
            <br/>
            <br/>

            <label style="font-size:14px">Promo Code (if applicable):</label>
            <input type="text" class="form-control statement" name="promo_code" id="promo_code"/>
            <input type="hidden" value="1" name="page" />
            <br/><br/>

            <button class="btn" type="submit" name="submit">Next</button>
            </form>
            </div>
		<br/>
        <hr/>
		<b>Tips	</b>
        <ul>
		<li>When should I start renewing my Domestic Workers Insurance

        <span class="glyphicon glyphicon-question-sign" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="AWG to provide copy"></span></li>
		<li>Is your Domestic Workers passport still valid through the renewal period
        <span class="glyphicon glyphicon-question-sign" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="AWG to provide copy"></span>
        </li>
		<li>What if my Domestic Worker is going for her home leave before continuing her contract with me
        <span class="glyphicon glyphicon-question-sign" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="AWG to provide copy"></span></li>

        <script type="text/javascript">
            jQuery(function () {
                jQuery(\'[data-toggle="tooltip"]\').tooltip()
            })
        </script>
		';
}

function displayPage2(){
    $start_date	=	$_POST['start_date'];
    $duration	=	$_POST['duration'];
    $nationality	=	$_POST['nationality'];
    $promo_code	=	$_POST['promo_code'];

    echo'
		<div class="progress">
		<div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">1. Select Plan</div>
		<div class="progress-bar" role="progressbar" style="width: 25%; background-color: #e9ecef" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">2. Select Add-Ons</div>
		<div class="progress-bar" role="progressbar" style="width: 25%; background-color: #e9ecef" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">3. Enter Particulars</div>
		<div class="progress-bar" role="progressbar" style="width: 25%; background-color: #e9ecef" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">4. Order Summary</div>
		</div>

		<p>Start Date: ' . $start_date . '</p>
		<p>Duration: ' . $duration . '</p>
		<p>Maid Nationality: ' . $nationality . '</p>';

    global $wpdb;
    $type_results = $wpdb->get_results( "SELECT name FROM mi_policy_type" );

    $pricing_results = $wpdb->get_results( "SELECT * FROM mi_policy_pricing
		inner join mi_policy_type
		on mi_policy_pricing.policy_id = mi_policy_type.policy_id
		inner join mi_policy_duration
		on mi_policy_pricing.duration_id = mi_policy_duration.duration_id
		where mi_policy_duration.duration like '$duration'
		and mi_policy_pricing.category like 'A'" );

    echo '
		<table class="table table-striped">
		<thead>
		<tr>
		<th scope="col"></th>
		';
    foreach ($type_results as $type_results) {
        echo '<th scope="col">' .$type_results->name. '</th>';
    }
    echo'
		</tr>
		</thead>
		<tbody>
		<tr>
		<td>Price</td>';
    foreach ($pricing_results as $prices) {
        echo '<td>S$' .$prices->pricing. '</td>';
    }
    echo
        '</tr>
		<tr>
		<td>Benefits</td><td colspan=3>' .$pricing_results[0]->benefits. '</td>
		</tr>
		<tr>
		<td>Outpatient Expenses due to Accident</td>';
    foreach ($pricing_results as $outpatient) {
        echo '<td>S$' .$outpatient->outpatient. '</td>';
    }
    echo'
		</tr>
		<tr>
		<td>Hospitalisation & surgical expenses per year</td>';
    foreach ($pricing_results as $hospitalisation) {
        echo '<td>S$' .$hospitalisation->hospitalisation. '</td>';
    }
    echo'
		</tr>
		<tr>
		<td>Re-hiring expenses</td>';
    foreach ($pricing_results as $rehiring) {
        echo '<td>S$' .$rehiring->rehiring. '</td>';
    }
    echo'
		</tr>
		<tr>
		<td>Liability to third parties</td>';
    foreach ($pricing_results as $liabilities) {
        echo '<td>S$' .$liabilities->liabilities. '</td>';
    }
    echo'
		</tr>
		<tr>
		<td>Damage or theft of maid\'s personal belongings</td>';
    foreach ($pricing_results as $damage_theft) {
        echo '<td>S$' .$damage_theft->damage_theft. '</td>';
    }
    echo'
		</tr>
		<tr>
		<td></td>
		<form method="post" action="' . $this_page .'">
		';
    foreach ($pricing_results as $pricing_id) {
        echo '<td><input type="radio" name="plan" value="' .$pricing_id->pricing_id. '"> ' .$pricing_id->name. '</td>';
    }
    echo'
		</tr>
		</tbody>
		</table>
		<input type="hidden" value="2" name="page" />
        <input type="hidden" value="' . $promo_code .'" name="promo_code" />
		<input type="hidden" value="' . $start_date .'" name="start_date" />
		<input type="hidden" value="' . $nationality .'" name="nationality" />
		<input type="submit" style="float:right"/>
		</form>
		<button onclick="history.go(-1);" style="float:left">Back</button>
		';
}

function displayPage3(){
    $plan	=	$_POST['plan'];
    $start_date	=	$_POST['start_date'];
    $nationality	=	$_POST['nationality'];
    $promo_code    =   $_POST['promo_code'];

    echo'
		<div class="progress">
		<div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">1. Select Plan</div>
		<div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">2. Select Add-Ons</div>
		<div class="progress-bar" role="progressbar" style="width: 25%; background-color: #e9ecef" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">3. Enter Particulars</div>
		<div class="progress-bar" role="progressbar" style="width: 25%; background-color: #e9ecef" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">4. Order Summary</div>
		</div>

		<h3>Select Add-Ons</h3>
		<p>Start Date: ' . $start_date . '</p>
		<p>Maid Nationality: ' . $nationality . '</p>

		<form method="post" action="' . $this_page .'">
		<table class="table table-striped">
		<thead>
		<tr>
		<th scope="col" style="width: 70%">Add-On</th><td>Premium</td><td></td>
		</tr>
		</thead>
		<tbody>
		<for
		<tr>
		<td><b>Indemnity Buy Back </b>
		<br/>Waiver of Indemnity for the forfeiture of $5000 security deposit to Ministry of Manpower, in the event the forfeiture is due to breach of conditions by the insured Maid.ss</td>
		<td>$50</td>
		<td><input type ="checkbox" name="add_on[]" value= "1"></input></td>
		</tr>
		<tr>
		<td><b>Daily Hospital Allowance Benefit </b>
		<br/>Receive Maid Allowance of $20 per day up to 15days if your Domestic Helper is hospitalised for 3 or more days due to an injury or illness. </td>
		<td>$6</td>
		<td><input type ="checkbox" name="add_on[]" value= "2"></input></td>
		</tr>
		<tr>
		<td><b>Daily Child/Elder Care Allowance Benefit</b>
		<br/>Receive Additional Allowance of $30 per day if you have Children below 12 years old, Elderly Above 65 years old when your Maid is Hospitalised. </td>
		<td>$30</td>
		<td><input type ="checkbox" name="add_on[]" value= "3"></input></td>
		</tr>
		<tr>
		<td><b>Outpatient Medical Expenses Due to Illness</b>
		<br/>Receive up to $30 per visit after you have paid $10 if your maid is not well and needs to visit the General Practioner. </td>
		<td>$78</td>
		<td><input type ="checkbox" name="add_on[]" value= "4"></input></td>
		</tr>
		<tr>
		<td><b>Top up Hospital & Surgical Expenses</b>
		<br/>Add additional coverage amount:
        <select id="benefit" name="benefit" />
		<option value="Please Select" selected>Please Select</option>
		<option value="5000">$5000</option>
		<option value="10000">$10000</option>
		<option value="15000">$15000</option>
		<option value="20000">$20000</option>
        <option value="25000">$20000</option>
		</select>
        </td>
		<td><div id="additional_amt">$--</div></td>
		<td><input id ="add_cb" name="add_on[]" type ="checkbox" disabled></input></td>
		</tr>
		<tr>
		<td><b>Home Content</b>
		<br/>Insure your Home Content against theft by your Maid. </td>
		<td>$50</td>
		<td><input type ="checkbox" name="add_on[]" value= "10"></input></td>
		</tr>
		</tbody>
		</table>
		<input type="hidden" value="3" name="page" />
        <input type="hidden" value="' . $plan .'" name="plan" />
        <input type="hidden" value="' . $promo_code .'" name="promo_code" />
		<input type="hidden" value="' . $start_date .'" name="start_date" />
		<input type="hidden" value="' . $nationality .'" name="nationality" />
		<input type="submit" style="float:right"/>
		</form>
		<button onclick="history.go(-1);" style="float:left">Back</button>
		<script type="text/javascript">
            jQuery(function() {
                jQuery(\'#benefit\').change(function(){
                    if (jQuery(\'#benefit\').val()=="5000"){
                        jQuery(\'#additional_amt\').text("$30");
                        jQuery(\'#add_cb\').removeAttr("disabled");
                        jQuery(\'#add_cb\').val("5");
                    }else if(jQuery(\'#benefit\').val()=="10000"){
                        jQuery(\'#additional_amt\').text("$55");
                        jQuery(\'#add_cb\').removeAttr("disabled");
                        jQuery(\'#add_cb\').val("6");
                    }else if(jQuery(\'#benefit\').val()=="15000"){
                        jQuery(\'#additional_amt\').text("$80");
                        jQuery(\'#add_cb\').removeAttr("disabled");
                        jQuery(\'#add_cb\').val("7");
                    }else if(jQuery(\'#benefit\').val()=="20000"){
                        jQuery(\'#additional_amt\').text("$105");
                        jQuery(\'#add_cb\').removeAttr("disabled");
                        jQuery(\'#add_cb\').val("8");
                    }else if(jQuery(\'#benefit\').val()=="25000"){
                        jQuery(\'#additional_amt\').text("$130");
                        jQuery(\'#add_cb\').removeAttr("disabled");
                        jQuery(\'#add_cb\').val("9");
                    }else if(jQuery(\'#benefit\').val()=="Please Select"){
                        jQuery(\'#additional_amt\').text("--");
                        jQuery(\'#add_cb\').attr("disabled", true);
                        jQuery(\'#add_cb\').prop(\'checked\', false)
                    }
                });
            });
        </script>
    ';
}

function displayPage4(){
    $plan	=	$_POST['plan'];
    $start_date	=	$_POST['start_date'];
    $nationality	=	$_POST['nationality'];
    $promo_code    =   $_POST['promo_code'];
    $add_ons        =   $_POST['add_on'];

    echo'
        <div class="progress">
		  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">1. Select Plan</div>
		  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">2. Select Add-Ons</div>
		  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">3. Enter Particulars</div>
		  <div class="progress-bar" role="progressbar" style="width: 25%; background-color: #e9ecef" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">4. Order Summary</div>
		</div>';


    echo'
        <form method="post" data-toggle="validator" action="' . $this_page .'">
            <div id = "personal">
                <h3> Personal Particulars</h3>
                <hr/>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="given_name" id="given_name_lbl">Given Name</label>
                    <input type="text" name="given_name" id="given_name" class="form-control" required/>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group col-md-6">
                    <label for="sur_name" id="sur_name_lbl">Surname</label>
                    <input type="text" name="sur_name" id="sur_name" class="form-control" required/>
                    <div class="help-block with-errors"></div>
                </div>

                    <div class="form-group col-md-12">
                        <label for="email" id="email_lbl">Email</label>
                        <input type="email" name="email" id="email" class="form-control" required/>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="phone_number" id="phone_number_lbl">Phone Number</label>
                        <div class="input-group">
                            <div class="input-group-addon">+65</div>
                            <input type="text" name="phone_number" id="phone_number" class="form-control" required/>
                        </div>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="dob" id="dob_lbl">Date of Birth</label>
                        <input type="date" name="dob" id="dob" class="form-control" required/>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="nric" id="nric_lbl">NRIC/FIN</label>
                        <input type="text" name="nric" id="nric" class="form-control" required/>
                        <div class="help-block with-errors"></div>
                    </div>

                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="unit_no" id="unit_no_lbl">Unit No.</label>
                        <input type="text" name="unit_no" id="unit_no" class="form-control"/>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="building" id="building_lbl">Building Name(if any)</label>
                        <input type="text" name="building" id="building" class="form-control"/>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="street_no" id="street_no_lbl">Street</label>
                        <input type="text" name="street_no" id="street_no" class="form-control" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="postal_code" id="postal_code_lbl">Postal Code</label>
                        <input type="text" name="postal_code" id="postal_code" class="form-control" required />
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="policy_start_date" id="policy_start_date_lbl">Policy Start Date</label>
                        <input type="date" name="policy_start_date" id="policy_start_date" class="form-control" value="'.$start_date.'" data-minDate required/>
                        <div class="help-block with-errors"></div>
                    </div>
            </div>

                <br/><br/>

            <div id = "maid" style="padding-top:5px">
                <h3> Maid Particulars</h3>
                <hr/>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="given_name" id="given_name_lbl">Given Name</label>
                    <input type="text" name="maid_given_name" id="maid_given_name" class="form-control" required/>
                    <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="sur_name" id="sur_name_lbl">Surname</label>
                        <input type="text" name="maid_sur_name" id="maid_sur_name" class="form-control" required/>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="passport" id="passport_lbl">Passport No.</label>
                        <input type="text" name="passport" id="passport" class="form-control" required/>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="work_permit" id="work_permit_lbl">Work Permit No.</label>
                        <input type="text" name="work_permit" id="work_permit" class="form-control" required/>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="work_permit_type" id="work_permit_type_lbl">Work Permit Type</label>
                        <select class="form-control" name="work_permit_type" id="work_permit_type" required/>
                            <option value ="" selected>Please Select</option>
                            <option>New</option>
                            <option>Transfer</option>
                            <option>Renewal</option>
                            <option>Not Sure</option>
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="dob_maid" id="dob_maid_lbl">Date of Birth</label>
                        <input type="date" name="dob_maid" id="dob_maid" class="form-control" required/>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="sb_trans" id="sb_trans_lbl">SB Transmission No.</label>
                        <input type="text" name="sb_trans" id="sb_trans" class="form-control" required />
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <input type="hidden" value="4" name="page" />
            <input type="hidden" value="' . $plan .'" name="plan" />
            <input type="hidden" value="' . $promo_code .'" name="promo_code" />
            <input type="hidden" value="' . $start_date .'" name="start_date" />
            <input type="hidden" value="' . $nationality .'" name="nationality" />';
    foreach ($add_ons as $value){
        echo '<input type="hidden" name="add_on[]" value=' . $value . '>';
    }
    echo'
            <button class="btn" type="submit" style="float:right"/>Submit</button>
            </form>
            <button class="btn btn-default" onclick="history.go(-1);" style="float:left">Back</button>
        ';
}

function displayPage5(){
    $plan	          =	$_POST['plan'];
    $nationality	  =	$_POST['nationality'];
    $promo_code       = $_POST['promo_code'];
    $add_ons          = $_POST['add_on'];
    $given_name	      =	$_POST['given_name'];
    $sur_name         =	$_POST['sur_name'];
    $email	          =	$_POST['email'];
    $phone_number     = $_POST['phone_number'];
    $dob              = $_POST['dob'];
    $nric             = $_POST['nric'];
    $unit_no          = $_POST['unit_no'];
    $building         = $_POST['building'];
    $street_no        = $_POST['street_no'];
    $postal_code      = $_POST['postal_code'];
    $policy_start_date=	$_POST['policy_start_date'];

    $maid_given_name  = $_POST['maid_given_name'];
    $maid_sur_name    = $_POST['maid_sur_name'];
    $passport         = $_POST['passport'];
    $work_permit      = $_POST['work_permit'];
    $work_permit_type = $_POST['work_permit_type'];
    $dob_maid         = $_POST['dob_maid'];
    $sb_trans         =	$_POST['sb_trans'];

    echo'
        <div class="progress">
		  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">1. Select Plan</div>
		  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">2. Select Add-Ons</div>
		  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">3. Enter Particulars</div>
		  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">4. Order Summary</div>
		</div>';

    echo'
    <h3> Order Summary </h3>
    <hr/>
    <div class="container">
        <div class="row">
            <div class="col-md-6" >';
    global $wpdb;
    $addon_result;

    $plan_result = $wpdb->get_row( "SELECT * FROM mi_policy_pricing inner join mi_policy_type on mi_policy_pricing.policy_id = mi_policy_type.policy_id where pricing_id =" . $plan);

    $policy_name = $plan_result->name;
    $policy_pricing = $plan_result->pricing;

    echo '
                <span>'.$policy_name.' Policy</span>
                <table class="table table-striped " style = "font-size:10px">
                    <tbody>
                        <tr>
                            <th scope="row"> Benefits </th>
                            <td colspan = "2">'.$plan_result->benefits.'</td>
                        </tr>
                        <tr>
                            <th scope="row"> Coverage </th>
                            <td colspan = "2">
                                <table>
                                <tr>
                                        <td> Outpatient Expenses due to Accident </td>
                                        <td>S$'.$plan_result->outpatient.'</td>
                                    </tr>
                                    <tr>
                                        <td> Hospitalisation & surgical expenses per year </td>
                                        <td>S$'.$plan_result->hospitalisation.'</td>
                                    </tr>
                                    <tr>
                                        <td> Re-hiring expenses </td>
                                        <td>S$'.$plan_result->rehiring.'</td>
                                    </tr>
                                    <tr>
                                        <td> Liability to third parties </td>
                                        <td>S$'.$plan_result->liabilities.'</td>
                                    </tr>
                                    <tr>
                                        <td> Damage or theft of maid\'s personal belongings </td>
                                        <td>S$'.$plan_result->damage_theft.'</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="success">
                            <th colspan = 3 style="text-align:right" scope = "col">Price: S$'.$policy_pricing.'</th>
                        </tr>
                    </tbody>
                </table>
                ';
    $total_addons;
    if(!empty($add_ons)){
        echo'
                    <span>Add-Ons Selected</span>
                    <table class="table table-striped" style = "font-size:10px">
                        <thead>
                        <tr><th colspan=2></th><th>Price</th></tr>
                        </thead>';
        foreach ($add_ons as $add_on){
            echo '
                            <tr>';
            $addon_result = $wpdb->get_row( "SELECT * FROM mi_addon where id =" . $add_on);
            $total_addons+=$addon_result->price;
            echo '
                            <th scope="row">'.$addon_result->name .'</th>
                                <td>'.$addon_result->description.'</td>
                                <td>S$'.$addon_result->price.'</td>
                            </tr>';
        }
        echo'
                            <tr class="success">
                                <th colspan = 3 style="text-align:right" scope = "col">Price: S$'.$total_addons.'</th>
                            </tr>
                    </table>';
    }

    $total_pricing = $policy_pricing + $total_addons;

    echo'
            <span>Policy Holder Information</span>
            <table class="table table-condensed" style = "font-size:10px" >
                <thead>
                <tr>
                <th colspan = 2> Personal Particulars </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row" width="30%">First Name</th><td>'.$given_name.'</td>
                </tr>
                <tr>
                    <th scope="row" width="30%">Family Name</th><td>'.$sur_name.'</td>
                </tr>
                <tr>
                    <th scope="row" width="30%">Email</th><td>'.$email.'</td>
                </tr>
                <tr>
                    <th scope="row" width="30%">Phone Number</th><td>'.$phone_number.'</td>
                </tr>
                <tr>
                    <th scope="row" width="30%">Date of Birth</th><td>'.$dob.'</td>
                </tr>
                <tr>
                    <th scope="row" width="30%">NRIC/Fin</th><td>'.$nric.'</td>
                </tr>
                <tr>
                    <th scope="row" width="30%">Address</th>
                    <td>'.$unit_no.'<br/>'
        .$building.'<br/>'
        .$street_no.'<br/>'
        .$postal_code.'<br/>
                    </td>
                </tr>
                </tbody>
                <thead>
                <tr>
                <th colspan = 2> Maid Particulars </th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row" width="30%">First Name</th><td>'.$maid_given_name.'</td>
                    </tr>
                    <tr>
                        <th scope="row" width="30%">Family Name</th><td>'.$maid_sur_name.'</td>
                    </tr>
                    <tr>
                        <th scope="row" width="30%">Passport No.</th><td>'.$passport.'</td>
                    </tr>
                    <tr>
                        <th scope="row" width="30%">Work Permit No.</th><td>'.$work_permit.'</td>
                    </tr>
                    <tr>
                        <th scope="row" width="30%">Work Permit Type</th><td>'.$work_permit_type.'</td>
                    </tr>
                    <tr>
                        <th scope="row" width="30%">Date of Birth</th><td>'.$dob_maid.'</td>
                    </tr>
                    <tr>
                        <th scope="row" width="30%">SB Transmission No.</th><td>'.$sb_trans.'</td>
                    </tr>
                </tbody>
            </table>
            <button onclick="history.go(-1);" style="float:left">Back</button>
            </div>

            <div class="col-md-2" style="border:1px solid">
                <table class = "table">
                <thead>
                <tr><th colspan=2>Amount Payable</th></tr>
                </thead>
                <tr>
                <th>Policy </th>
                <td>$'.$policy_pricing.'</td></tr>';

    if(!is_null($total_addons)){
        echo '<tr><th>Add-Ons</th>
                    <td>$'.$total_addons.'</td></tr>';
    }

    $discount_amount = 0;
    if(!is_null($promo_code)){
        echo '<tr class = "warning"><th>'.$promo_code.'</th>';
        $promo_result = $wpdb->get_row( "SELECT * FROM mi_promo_code where id like '" . $promo_code."'");
        if($promo_result->type == "percent"){
            $discount_amount = round((($promo_result->amount)/100 * $total_pricing),2);
        }else if($promo_result->type == "amount"){
            $discount_amount = $promo_result;
        }
        echo '<td>-S$'.$discount_amount.'</td>';
    }
    $total_pricing = $total_pricing - $discount_amount;
    echo'
                <tr class = "success"><th>Total</th>
                <td>$'.$total_pricing.'</td></tr></table>
                <form
                 <form id = "paymentForm" name="paymentForm" method="post" action="https://staging.sgepay.com:8008/apisubmit.aspx" >

                <input type="hidden" name="MID" id="MID" />
                <input type="hidden" name="requestId" id="requestId" />
                <input type="hidden" name="BillNo" id="BillNo" />
                <input type="hidden" name="email" id="email" />
                <input type="hidden" name="amount" id="amount" />


                <input type = "submit" value = "Pay" id="paymentButton" />
                </form>
            </div>
        </div>
    </div>
    <script>
    jQuery("#paymentButton").click(function(e) {
        e.preventDefault(); //This will prevent form from submitting
jQuery.ajax({
    type: "GET",
    url: "wp-json/maidinsurance/v1/makepayment",
    dataType: "json",
    //data: {page: "5"},

    success: function (obj, textstatus) {

        var data = JSON.parse(obj);
        jQuery("#paymentForm #MID").val(data.MID);
        jQuery("#paymentForm #requestId").val(data.requestId);
        jQuery("#paymentForm #BillNo").val(data.BillNo);
        jQuery("#paymentForm #email").val(data.email);
        jQuery("#paymentForm #amount").val(data.amount);
        jQuery("#paymentForm").submit();   //Send client to the payment processor
            }
});
});
</script>

    ';



}
?>
