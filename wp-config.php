<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'awg_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R2G)?NzofTG[Rm#U:vaUdSuo-)2N!jRP5<&(y&w1} !l?4)g;.[SJ}3.4=0Nl)J#');
define('SECURE_AUTH_KEY',  'Q_1FSV|wGi Sbel#~M[@&)8u-TQkES<!0jD<+~ OU,>(X_AQfFCr7+Y^k:8Kz]X]');
define('LOGGED_IN_KEY',    'IEf[tsKF<Jm#P#RS!L4aX_UAppg]fL_exeiOeg(q}4pdOMxyqe]f(hC8S2}vCW$Q');
define('NONCE_KEY',        ' CT}{-Zx@,/ice7+n-fT{n1q0O_hzt@A4$Ni17m|jZ[::FCLlar!pp6qxa#D^|d]');
define('AUTH_SALT',        'GeiX$o{!)iTx.umi^ kRC;+D3*Knf1vkwYDw/j3h{4<s@CC8-xag8t}[w.hYAs%)');
define('SECURE_AUTH_SALT', ')y=v:dwS|Nd3*~aRqvo5IXE;;C/{iD!$}tmjxb+kPy%B0&0$s`L0,(iS>u-t8J&r');
define('LOGGED_IN_SALT',   'H:p&zAL{f&p&vjcovd;6>hiqA}v2e^<t.?~Gg_*t.Bg5uq57c3>~$rOJCM`lBp8 ');
define('NONCE_SALT',       'zBeb>+3hH N(+P.BE6|?0qVS_)?#Q^8D!,`=%;CJ|[:H?9o=s%1/v#$1MD8W<0Vo');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
